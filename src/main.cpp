// clang++ main.cpp -o prog -lglfw -lGL -lX11 -lpthread -lXrandr -lXi -ldl
// followed this tutorial : https://learnopengl.com/

#define STB_IMAGE_IMPLEMENTATION

#include "glad/glad.h"
#include "glad.c" // edited glad.c to include "glad..." instead of <glad...>. I really have to build a clean MakeFile !
#include <GLFW/glfw3.h> // sudo apt-get install glfw3-dev
#include "stb_image.h"
//#include "stb/stb_image.h"
#include <iostream>
#include <fstream>
#include <sstream>

#include <stdio.h>	  /* printf, scanf, puts, NULL */
#include <stdlib.h>	 /* srand, rand */ // sudo apt-get install libxi-dev
#include <time.h>	   /* time */
#include <vector>
#include <cmath>

#include <glm/glm.hpp> // sudo apt-get install libglm-dev
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "camera.h"
#include "shader_m.h"

#include "Frame.hpp"
#include "Entity.hpp"

//###########
// Classes & Structures

/*class ObjectsLoader
{
private:
	std::vector<TexturedEntity *> _textEnt;

public:
	void loadFromFile(std::string adress);

	void draw();
}*/


class Star
{
private:
	// POSITIONING
	glm::vec3 _center;
	float _radius;
	float _revolutionTime;
	glm::vec3 _axis1;
	glm::vec3 _axis2;
	bool _clockwise;

	// ROTATING
	glm::vec3 _axis;
	float _angle;
	float _dayTime;

	// UPDATING
	glm::vec3 _coords;
	float _momentum;

public:
	Star(glm::vec3 center, glm::vec3 ax1, glm::vec3 ax2, float radius, float revoTime, bool clockwise, float moment);

	void setMomentum(float moment){_momentum = moment;}
	void update(float deltaTime);

	void updatePosition();

	glm::vec3 getCoords(){return _coords;}

};

//###########
// Méthodes

// Réglages initiaux d'OpenGL (version, utilisation du Core, taille de fenêtre...)
int createWindow(unsigned int width, unsigned int height);

// Entrée des fonctions Callback dans OpenGL
void setCallbacks();

// Gestion des inputs
void processInput(GLFWwindow *window);

// Fonctions utilisées automatiquement par OpenGL
void framebuffer_size_callback(GLFWwindow* window, int width, int height);
void mouse_callback(GLFWwindow* window, double xpos, double ypos);
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset);

// Création d'objets
void loadObj(std::vector<float> & vertices, std::vector<int> & indices, std::string adress);
void loadObj(std::vector<float> & vertices, std::string adress);

void loadTexture(unsigned int & texture, std::string adress, std::string format, bool verticalFlip);

void bind_and_configure(unsigned int & VBO, unsigned int & VAO, unsigned int & VEO, int* attribs, int nbAttribs, int extraAttribs, bool useIndices, float* vertices, int nbVert, int* indices, int nbInd);

// Gestion des objets

void drawSet(Shader & shader, unsigned int & VAO, int nbVert, int nbObj, glm::vec3* positions, glm::vec3* scaling, float* angles, glm::vec3* axes, bool ind);

//###########
// variables globales

GLFWwindow* window;

// settings
const unsigned int SCR_WIDTH = 1080; //autoriser les modifications de ces variables
const unsigned int SCR_HEIGHT = 720; // mais appliquer immédiatement ces modifications à la fenêtre

bool firstMouse = true;
Camera camera(glm::vec3(0.0f, 0.0f, 3.0f));

// timing
float deltaTime = 0.0f;	// time between current frame and last frame
float lastFrame = 0.0f;



int main()
{
	/* initialize random seed: */
	srand (time(NULL));

	if(!createWindow(SCR_WIDTH, SCR_HEIGHT))
		return -1;

	setCallbacks();

	// glad: load all OpenGL function pointers
	// ---------------------------------------
	// GLAD manages function pointers for OpenGL so we want to initialize GLAD before we call any OpenGL function:
	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		std::cout << "Failed to initialize GLAD" << std::endl;
		return -1;
	}

	//#####################################################################
	// configure global opengl state
	// -----------------------------
	glEnable(GL_DEPTH_TEST);

	//#####################################################################
	// build and compile our shader program
	// ------------------------------------
	Shader textShader = Shader("shaders/texture_vs.cpp", "shaders/texture_fs.cpp"); // you can name your shader files however you like
	//Shader coloredShader("shaders/color_vs.cpp", "shaders/color_fs.cpp");
	Shader lampShader("shaders/lamp_vs.cpp", "shaders/lamp_fs.cpp");

	//#####################################################################
	// set up vertex data (and buffer(s)) and configure vertex attributes
	// ------------------------------------------------------------------

	int nbLights = 2;
	glm::vec3 lightPositions[] = {
		glm::vec3( -3.0f,  -1.0f,  0.0f),
		glm::vec3( 5.0f,  1.0f,  -4.0f)
	};
	glm::vec3 lightScale[] = {
		glm::vec3(4.f),
		glm::vec3(2.f)
	};

	//Star(center, axis1, axis2, radius, revolutionTime, isClockwise, moment)
	Star star1 = Star(glm::vec3(0,0,0), glm::vec3(1,0,0), glm::vec3(0,1,0.2), 200, 50, true, 0);
	Star star2 = Star(glm::vec3(0,0,0), glm::vec3(1,0,0), glm::vec3(0,1,0.2), 100, 50, true, 25);

	Frame fr_isle("data/isle.obj");
	Frame fr_cone("data/cone.obj");
	Frame fr_dome("data/demi-sphere1.obj");
	Frame fr_sphere("data/sphere.obj");
	Frame fr_thing("data/little_thing.obj");

	//TexturedEntity ent(frame, shader, pos, scale, axis, angle);
	TexturedEntity isleUp(&fr_isle, &textShader, glm::vec3( 0.0f,  -10.0f,  0.0f), glm::vec3(1.f,1.f,1.f), glm::vec3(0.f,1.f,0.f), 0.f);
	TexturedEntity isleDown(&fr_isle, &textShader, glm::vec3( 0.0f,  -10.01f,  0.0f), glm::vec3(1.f,1.f,1.f), glm::vec3(0.f,1.f,0.f), 0.f);

	TexturedEntity theThing(&fr_thing, &textShader, glm::vec3( 1.0f,  -7.5f,  -5.0f), glm::vec3(1.f,1.f,1.f), glm::vec3(0.f,1.f,0.f), 0.f);

	TexturedEntity ball(&fr_sphere, &textShader, glm::vec3( -0.5f,  -8.5f,  3.0f), glm::vec3(1.f,1.f,1.f), glm::vec3(0.f,1.f,0.f), 0.f);

	TexturedEntity cone(&fr_cone, &textShader, glm::vec3( -1.0f,  -8.5f,  -2.0f), glm::vec3(1.f,1.f,1.f), glm::vec3(0.f,1.f,0.f), 0.f);

	TexturedEntity dome1(&fr_dome, &textShader, glm::vec3( 0.0f,  -10.0f,  0.0f), glm::vec3(.1f,1.1f,.1f), glm::vec3(0.f,1.f,0.f), 0.f);
	TexturedEntity dome2(&fr_dome, &textShader, glm::vec3( -3.0f,  -11.0f,  2.0f), glm::vec3(.2f,.8f,.4f), glm::vec3(0.f,1.f,0.f), 0.f);
	TexturedEntity dome3(&fr_dome, &textShader, glm::vec3( 4.0f,  -10.0f,  3.0f), glm::vec3(.3f,1.1f,.1f), glm::vec3(0.f,1.f,0.f), 0.f);
	TexturedEntity dome4(&fr_dome, &textShader, glm::vec3( 2.0f,  -9.3f,  -4.0f), glm::vec3(.3f,.2f,.3f), glm::vec3(0.f,1.f,0.f), 0.f);

	//################################
	// load and create a texture
	// -------------------------
	unsigned int texture1, texture2, texture3, texture4, texture5, texture6;

	loadTexture(texture1, "data/ground1.png", "png", false);
	loadTexture(texture2, "data/ground2.png", "png", false);
	loadTexture(texture3, "data/marble.png", "png", false);

	textShader.use();

	//textShader.setInt("texture1", texture1);
	//textShader.setInt("texture2", texture2);

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, texture1);
	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, texture2);
	glActiveTexture(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_2D, texture3);

	theThing.setTextures(texture1, 1.6f, texture2, 0.1f);
	theThing.setTextureZoom(0.2f);
	ball.setLoneTexture(texture3, 0.8f);
	isleUp.setTextures(texture1, 0.8f, texture2, 0.f);
	isleUp.setTextureZoom(0.2f);
	isleDown.setLoneTexture(texture3, 0.8f);
	isleDown.setTextureZoom(0.5f);
	cone.setLoneTexture(texture3, 0.8f);
	cone.setTextureZoom(0.5f);

	dome1.setTextures(texture1, 0.8f, texture2, 0.f);
	dome1.setTextureZoom(0.2f);
	dome2.setTextures(texture1, 0.8f, texture2, 0.f);
	dome2.setTextureZoom(0.2f);
	dome3.setTextures(texture1, 0.8f, texture2, 0.f);
	dome3.setTextureZoom(0.2f);
	dome4.setTextures(texture1, 0.8f, texture2, 0.f);
	dome4.setTextureZoom(0.2f);

	//######################################################
	// uncomment this call to draw in wireframe polygons.
	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	//###################
	// render loop
	while(!glfwWindowShouldClose(window))
	{
		// per-frame time logic
		// --------------------

		float currentFrame = glfwGetTime();
		deltaTime = currentFrame - lastFrame;
		lastFrame = currentFrame;

		star1.update(deltaTime);
		star2.update(deltaTime);

		lightPositions[0] = star1.getCoords();
		lightPositions[1] = star2.getCoords();

		//#######
		// input
		processInput(window);

		//########
		// render
		glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		//###############
		// POV Managing

		// view/projection transformations
    glm::mat4 projection = glm::perspective(glm::radians(camera.Zoom), (float)SCR_WIDTH / (float)SCR_HEIGHT, 0.1f, 1000.0f);
    glm::mat4 view = camera.GetViewMatrix();

		textShader.use();
    textShader.setMat4("projection", projection);
		textShader.setMat4("view", view);
		//coloredShader.use();
    //coloredShader.setMat4("projection", projection);
		//coloredShader.setMat4("view", view);
		lampShader.use();
    lampShader.setMat4("projection", projection);
		lampShader.setMat4("view", view);

		//#################
		// Light Managing
		textShader.use();

		textShader.setInt("nbLights", 2);
		textShader.setVec3("ambiantLight", 1.0f, 1.0f, 1.0f);
		textShader.setVec3("viewPos", camera.Position);

		textShader.setVec3("lightColor1", 1.0f, 0.9f, 0.7f);
		textShader.setVec3("lightPos1", lightPositions[0]);
		textShader.setVec3("lightColor2", 0.0f, 0.3f, 0.5f);
		textShader.setVec3("lightPos2", lightPositions[1]);

		//##################
		// DRAWING
		lampShader.use();

		drawSet(lampShader, fr_sphere.getVAO(), fr_sphere.getVertices().size(), 2, lightPositions, lightScale, NULL, NULL, false);

		theThing.draw();
		ball.draw();
		isleUp.draw();
		isleDown.draw();
		cone.draw();

		dome1.draw();
		dome2.draw();
		dome3.draw();
		dome4.draw();

		//#################################################################################
		// glfw: swap buffers and poll IO events (keys pressed/released, mouse moved etc.)
		glfwSwapBuffers(window);
		glfwPollEvents();

		//glfwSetCursorPos(window,SCR_WIDTH/2,SCR_HEIGHT/2);
	}

	//#################################################################################
	// optional: de-allocate all resources once they've outlived their purpose:
	// ------------------------------------------------------------------------
	//glDeleteVertexArrays(1, &VAO);
	//glDeleteBuffers(1, &VBO);

	//#####################################################################
	// glfw: terminate, clearing all previously allocated GLFW resources.
	// ------------------------------------------------------------------
	glfwTerminate();
	return 0;
}

// setting OpenGL's version and window settings
// ---------------------------------------------------------------------------------------------------------
int createWindow(unsigned int width, unsigned int height)
{
	// glfw: initialize and configure
	// ------------------------------
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3); // set openGL to version 3.x
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3); // set openGL to version x.3, which leads to 3.3
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE); //please, use the Core-Profile
	//-> core-profile means we'll get access to a smaller subset of OpenGL features without backwards-compatible features we no longer need

	#ifdef __APPLE__
		glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // compilation on OS X
	#endif

	// glfw window creation
	// --------------------
	window = glfwCreateWindow(width, height, "My Own Thing", NULL, NULL);

	if (window == NULL)
	{
		std::cout << "Failed to create GLFW window" << std::endl;
		glfwTerminate();
		return 0;
	}

	glfwMakeContextCurrent(window);

	return 1;
}

// loading a texture
// -------------------
void loadTexture(unsigned int & texture, std::string adress, std::string format, bool verticalFlip)
{
	unsigned char *data;
	int width, height, nrChannels;

	// ---------
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	 // set the texture wrapping parameters
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);	// set texture wrapping to GL_REPEAT (default wrapping method)
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	// set texture filtering parameters
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	// load image, create texture and generate mipmaps
	stbi_set_flip_vertically_on_load(verticalFlip); // tell stb_image.h to flip loaded texture's on the y-axis.
	// The FileSystem::getPath(...) is part of the GitHub repository so we can find files on any IDE/platform; replace it with your own image path.
	data = stbi_load(adress.c_str(), &width, &height, &nrChannels, 0);
	if (data)
	{
		if(format == "jpg")
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
		else if(format == "png")
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
		glGenerateMipmap(GL_TEXTURE_2D);
	}
	else
	{
		std::cout << "Failed to load texture" << std::endl;
	}
	stbi_image_free(data);
}

// read an .obj file exported from Blender
// -----------------------------------------
void loadObj(std::vector<float> & vertices, std::vector<int> & indices, std::string adress)
{
	std::ifstream file(adress); //1543 800
	std::istringstream iss;

	vertices.clear();
	indices.clear();

	if(file)
	{
		std::string line;

		bool facesTurn = false;

		int c;
		float x, y, z;
		std::string pref;
		std::string seg;
		int nbVertices = 0; //num of the corresponding cell of the map
		int nbTriangles = 0;
		std::vector<int> segList;
		while( std::getline(file, line) )
        {
			std::istringstream iss(line);

			iss >> pref;
			if(pref == "v"){
				if(!(iss >> x >> y >> z)) { std::cout << "x, y, z not found"; break;}
				++nbVertices;
				vertices.push_back((float)x);
				vertices.push_back((float)y);
				vertices.push_back((float)z);
				vertices.push_back((float)x+(float)z); //or adding textures floats later ?
				vertices.push_back((float)y+(float)z);
			} else if (pref == "f") {
				if(!facesTurn){
					facesTurn = true;
				}
				segList.clear();

				nbTriangles = -2;
				while(iss >> seg){ //ex of seg : 12//8 ; we want the 12
					c=0;
					int i=0;
					do{
						c *= 10;
						c += seg[i] - '0';
						++i;
					}while(seg[i]!='/');

					segList.push_back(c-1);
					++nbTriangles;
				}
				//nbTriangles = segList.size() - 2;

				for(int i=0; i<nbTriangles; ++i){
					indices.push_back(segList[0]);
					indices.push_back(segList[i+1]);
					indices.push_back(segList[i+2]);
				}
			}
		}

		file.close();
	}
	else
		std::cout << "Erreur lors du chargement du fichier "<< adress << std::endl;//erreur
}

// read an .obj file exported from Blender
// -----------------------------------------
/*
# Blender v2.79 (sub 0) OBJ File: ''
# www.blender.org
mtllib little_thing.mtl
o Cube
v 1.000000 -1.539769 -1.384272
(v -> vertex coords)
(...)
vn -0.2606 -0.9655 0.0000
(vn -> face normal vertor)
(...)
usemtl Material
s off
(don't know what's that shit)
f 1//1 2//1 3//1 4//1
(f -> face's vertices indices)
*/
void loadObj(std::vector<float> & verts, std::string adress)
{
	std::ifstream file(adress); //1543 800
	std::istringstream iss;

	std::vector<float> vertices;
	std::vector<float> normals;

	verts = std::vector<float>();

	if(file)
	{
		std::string line;

		int c;
		float x, y, z;
		std::string pref;
		std::string seg;
		int nbVertices = 0; //num of the corresponding cell of the map
		int nbTriangles = 0;
    int nbFaces = 0;
		std::vector<int> segList;
		while( std::getline(file, line) )
    {
			std::istringstream iss(line);

			iss >> pref;
			if(pref == "v"){
				if(!(iss >> x >> y >> z)) { std::cout << "x, y, z not found"; break;}
				++nbVertices;
				vertices.push_back(x);
				vertices.push_back(y);
				vertices.push_back(z);
			} else if (pref == "vn") {
        if(!(iss >> x >> y >> z)) { std::cout << "x, y, z not found"; break;}
        normals.push_back(x);
        normals.push_back(y);
        normals.push_back(z);
			} else if (pref == "f") {
				segList.clear();

				nbTriangles = -2;
				while(iss >> seg){ //ex of seg : 12//8 ; we want the 12
					c=0;
					int i=0;
					do{
						c *= 10;
						c += seg[i] - '0';
						++i;
					}while(seg[i]!='/');

					segList.push_back(c-1);
					++nbTriangles;
				}
				//nbTriangles = segList.size() - 2;

				for(int i=0; i<nbTriangles; ++i){
          // segList : list of indices, i.e of vertices ;
          // a vertice : 3 floats, i.e x, y, z
          // what I am doing here : adding a triangle, i.e 3 vertices and their normal vectors
          // more specifically : x1, y1, z1, n, n, n, ..., x3, y3, z3, n, n, n;
          // I could add textures floats, initialized to zero

          verts.push_back(vertices[ (segList[0]*3)+0 ]); //faut faire un *3 là aussi...
          verts.push_back(vertices[ (segList[0]*3)+1 ]); //faut faire un *3 là aussi...
          verts.push_back(vertices[ (segList[0]*3)+2 ]); //faut faire un *3 là aussi...
          verts.push_back(0.0f); // 2 floats for texturing purpose
          verts.push_back(0.0f);
          verts.push_back(normals[ (nbFaces*3)+0 ]);
          verts.push_back(normals[ (nbFaces*3)+1 ]);
          verts.push_back(normals[ (nbFaces*3)+2 ]);

          verts.push_back(vertices[ (segList[i+1]*3)+0 ]); //faut faire un *3 là aussi...
          verts.push_back(vertices[ (segList[i+1]*3)+1 ]); //faut faire un *3 là aussi...
          verts.push_back(vertices[ (segList[i+1]*3)+2 ]); //faut faire un *3 là aussi...
          verts.push_back(0.0f);
          verts.push_back(0.0f);
          verts.push_back(normals[ (nbFaces*3)+0 ]);
          verts.push_back(normals[ (nbFaces*3)+1 ]);
          verts.push_back(normals[ (nbFaces*3)+2 ]);

          verts.push_back(vertices[ (segList[i+2]*3)+0 ]); //faut faire un *3 là aussi...
          verts.push_back(vertices[ (segList[i+2]*3)+1 ]); //faut faire un *3 là aussi...
          verts.push_back(vertices[ (segList[i+2]*3)+2 ]); //faut faire un *3 là aussi...
          verts.push_back(0.0f);
          verts.push_back(0.0f);
          verts.push_back(normals[ (nbFaces*3)+0 ]);
          verts.push_back(normals[ (nbFaces*3)+1 ]);
          verts.push_back(normals[ (nbFaces*3)+2 ]);

          // ps : far too much the same normal vector (it doesn't change in the entire for loop)

				}
        ++nbFaces;
			}
		}

		file.close();
	}
	else
		std::cout << "Erreur lors du chargement du fichier "<< adress << std::endl;//erreur
}

// Bind things, configure things...
// ---------------------------------
// À revoir l'utilisation des VBO... dans quel cas on peut en réutiliser un, etc...
void bind_and_configure(unsigned int & VBO, unsigned int & VAO, unsigned int & EBO, int* attribs, int nbAttribs, int extraAttribs, bool useIndices, float* vertices, int nbVertices, int* indices, int nbIndices)
{
	int totalSize = 0;
	for(int i=0; i<nbAttribs; ++i)
	totalSize += attribs[i];
	totalSize += extraAttribs;

	glGenVertexArrays(1, &VAO);
	glGenBuffers(1, &VBO);
  if(useIndices)
		glGenBuffers(1, &EBO);

	glBindVertexArray(VAO); // bind the Vertex Array Object first,

	glBindBuffer(GL_ARRAY_BUFFER, VBO); // then bind and set vertex buffer(s),
	glBufferData(GL_ARRAY_BUFFER, nbVertices*sizeof(float), vertices, GL_STATIC_DRAW); // and then configure vertex attributes(s).

	if(useIndices){
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, nbIndices*sizeof(int), indices, GL_STATIC_DRAW);
	}

	int stride=0;
	for(int i=0; i<nbAttribs; ++i){
		glVertexAttribPointer(i, attribs[i], GL_FLOAT, GL_FALSE, totalSize * sizeof(float), (void*)(stride*sizeof(float)));
		glEnableVertexAttribArray(i);
		stride += attribs[i];
	}
}

// Draw a set of objets bind to a VAO at a said position
// ------------------------------------------------------
void drawSet(Shader & shader, unsigned int & VAO, int nbDraw, int nbObj, glm::vec3* positions, glm::vec3* scaling, float* angles, glm::vec3* axes, bool ind)
{
	shader.use();

	glm::mat4 model;
	glBindVertexArray(VAO);

	for (unsigned int i = 0; i < nbObj; i++)
	{
		// calculate the model matrix for each object and pass it to shader before drawing
		model = glm::mat4(1.0f);
		if(angles != NULL and axes != NULL)
			model = glm::rotate(model, glm::radians(angles[i]), axes[i]);
		if(positions != NULL)
			model = glm::translate(model, positions[i]);
		if(scaling != NULL)
			model = glm::scale(model, scaling[i]);
		shader.setMat4("model", model);

		if(ind)
			glDrawElements(GL_TRIANGLES, nbDraw, GL_UNSIGNED_INT, 0); //nbDraw = nb of indices
		else
			glDrawArrays(GL_TRIANGLES, 0, nbDraw); //nbDraw = nb of vertices
	}
}


// process all input: query GLFW whether relevant keys are pressed/released this frame and react accordingly
// ---------------------------------------------------------------------------------------------------------
void processInput(GLFWwindow *window)
{
	float cameraSpeed = 2.5 * deltaTime;

	if(glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		glfwSetWindowShouldClose(window, true);

	if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
    camera.ProcessKeyboard(FORWARD, deltaTime);
  if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
    camera.ProcessKeyboard(BACKWARD, deltaTime);
  if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
    camera.ProcessKeyboard(PORT, deltaTime);
  if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
    camera.ProcessKeyboard(STARBOARD, deltaTime);

	if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS)
	  camera.ProcessKeyboard(UP, deltaTime);
	if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS)
	 	camera.ProcessKeyboard(DOWN, deltaTime);
	if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS)
	 	camera.ProcessKeyboard(LEFT, deltaTime);
	if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS)
	  camera.ProcessKeyboard(RIGHT, deltaTime);

	if(glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS)
		camera.ProcessKeyboard(SPACE, deltaTime);
	if(glfwGetKey(window, GLFW_KEY_LEFT_CONTROL) == GLFW_PRESS)
		camera.ProcessKeyboard(CTRL, deltaTime);

	if(glfwGetKey(window, GLFW_KEY_F) == GLFW_PRESS) //need a pressed/released flag
		camera.changeFlatFront(true);
	else
		camera.changeFlatFront(false);

	/*if(glfwGetKey(window, GLFW_KEY_Z) == GLFW_PRESS)
		nearPlane *= 0.95;
	if(glfwGetKey(window, GLFW_KEY_X) == GLFW_PRESS)
		nearPlane *= 1.05;
	if(glfwGetKey(window, GLFW_KEY_C) == GLFW_PRESS)
		farPlane *= 0.95;
	if(glfwGetKey(window, GLFW_KEY_V) == GLFW_PRESS)
		farPlane *= 1.05;*/

	/*if(glfwGetKey(window, GLFW_KEY_KP_ADD) == GLFW_PRESS)
		fov*=1.1f;
	if(glfwGetKey(window, GLFW_KEY_KP_SUBTRACT) == GLFW_PRESS)
		fov*=0.9f;*/
}

//######################################loadFromFile(std::string adress)
// CALLBACK FUNCTIONS

// setting OpenGL's callback functions
// -------------------------------------
void setCallbacks(){
	// Moments chelous avec passages de fonctions en paramètres de fonctions...
	glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
	glfwSetCursorPosCallback(window, mouse_callback);
	glfwSetScrollCallback(window, scroll_callback);
}

// glfw: whenever the window size changed (by OS or user resize) this callback function executes
// ---------------------------------------------------------------------------------------------
void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
	// make sure the viewport matches the new window dimensions; note that width and
	// height will be significantly larger than specified on retina displays.
	glViewport(0, 0, width, height);
}


// glfw: whenever the mouse moves, this callback is called
// -------------------------------------------------------
void mouse_callback(GLFWwindow* window, double xpos, double ypos)
{
  if (!firstMouse)
  {
    float xoffset = xpos - SCR_WIDTH/2;
  	float yoffset = SCR_HEIGHT/2 - ypos; // reversed since y-coordinates go from bottom to top
		glfwSetCursorPos(window, SCR_WIDTH/2,SCR_HEIGHT/2); //SCR_WIDTH SCR_HEIGHT

		camera.ProcessMouseMovement(xoffset, yoffset);
	}
	else
		firstMouse = false;
}

// glfw: whenever the mouse scroll wheel scrolls, this callback is called
// ----------------------------------------------------------------------
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset)
{
  camera.ProcessMouseScroll(yoffset);
}

//#######################
// STAR CLASS

Star::Star(glm::vec3 center, glm::vec3 ax1, glm::vec3 ax2, float radius, float revoTime, bool clockwise, float moment) : _center(center), _radius(radius), _revolutionTime(revoTime), _axis1(ax1), _axis2(ax2), _clockwise(clockwise), _momentum(moment)
{
	float pi = acos(-1);

	float angle = pi*_momentum/_revolutionTime;
	float cosin = cosf(angle);
	float sinus = sinf(angle);

	_coords = (_axis1*cosin + _axis2*sinus)*_radius + _center;
}

void Star::update(float deltaTime)
{
	_momentum += deltaTime;

	if(_momentum >= _revolutionTime)
		_momentum -= _revolutionTime;

	updatePosition();
}

void Star::updatePosition(){
	float pi = acos(-1);

	float angle = 2*pi*_momentum/_revolutionTime;

	float cosin = cosf(angle);
	float sinus = sinf(angle);

	_coords = (_axis1*cosin + _axis2*sinus)*_radius + _center;
}


//##################
// LOADER CLASS

/*void ObjectsLoader::loadFromFile(std::string adress){
	std::ifstream file(adress); //1543 800
	std::istringstream iss;

	if(file)
	{
		std::string line;

		int c;
		float x, y, z;
		float width, height, depth;
		float axX, axY, axZ;
		float angle;
		std::string class;
		std::string frame;
		int nbVertices = 0; //num of the corresponding cell of the map
		int nbTriangles = 0;
		std::vector<int> segList;
		while( std::getline(file, line) )
	  {
			std::istringstream iss(line);

			iss >> class;
			if(pref == "TexturedEntity"){
				//Shader should be automatically textShader

				iss >> frame;

				iss >> x >> y >> z >> width >> height >> depth >> axX >> axY >> axZ >> angle;

				//_textEnt.push_back(new TexturedEntity( glm::vec3(x,y,z), glm ));

			}

			file.close();
		}
		else
			std::cout << "Erreur lors du chargement du fichier "<< adress << std::endl;//erreur
	}
}*/
