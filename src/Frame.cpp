#include "Frame.hpp"

void loadObj(std::vector<float> & vertices, std::string adress);
void bind_and_configure(unsigned int & VBO, unsigned int & VAO, unsigned int & VEO, int* attribs, int nbAttribs, int extraAttribs, bool useIndices, float* vertices, int nbVert, int* indices, int nbInd);

//#####################
// BASE CLASS

Frame::Frame(std::string adress)
{
	loadObj(_vertices, adress);

	int attribs[] = {3,2,3};

	//bind_and_configure(VBO, VAO, EBO, floatsAttribs, nbFloats, extraFloats, useIndices, vertices, nbVert, indices, nbInd);
	bind_and_configure(_VBO, _VAO, _EBO, attribs, 3, 0, false, _vertices.data(), _vertices.size(), NULL, 0);
}

//#####################""
// SQUARED FRAME

/*SquaredFrame::SquaredFrame(){

}*/
