#include "Entity.hpp"

void loadObj(std::vector<float> & vertices, std::string adress);
void bind_and_configure(unsigned int & VBO, unsigned int & VAO, unsigned int & VEO, int* attribs, int nbAttribs, int extraAttribs, bool useIndices, float* vertices, int nbVert, int* indices, int nbInd);

//######################
// TEXTURED ENTITY

TexturedEntity::TexturedEntity(Frame * frame, Shader * shader, glm::vec3 pos, glm::vec3 scale, glm::vec3 axis, float angle){
	_frame = frame;
	_shader = shader;
	_coords = pos;
	_scale = scale;
	_axis = axis;
	_angle = angle;

	_textureZoom = 1.0f;
}

void TexturedEntity::setTextures(unsigned int text1, float spec1, unsigned int text2, float spec2){
	_textures[0] = text1;
	_speculars[0] = spec1;

	_textures[1] = text2;
	_speculars[1] = spec2;

	_nbText = 2;
}

void TexturedEntity::setLoneTexture(unsigned int text, float spec){
	_textures[0] = text;
	_speculars[0] = spec;

	_textures[1] = text;
	_speculars[1] = spec;

	_nbText = 1;
}

void TexturedEntity::draw(){
	glm::mat4 model;
	_shader->use();

	_shader->setInt("nbText", _nbText);
	_shader->setFloat("textureZoom", _textureZoom);

	_shader->setInt("texture1", _textures[0]);
	_shader->setInt("texture2", _textures[1]);

	_shader->setFloat("specularity1", _speculars[0]);
	_shader->setFloat("specularity2", _speculars[1]);

	glBindVertexArray(_frame->getVAO());

	model = glm::mat4(1.0f);
	//model = glm::rotate(model, glm::radians(_angle), _axis);
	model = glm::translate(model, _coords);
	model = glm::scale(model, _scale);
	_shader->setMat4("model", model);

	glDrawArrays(GL_TRIANGLES, 0, _frame->getVertices().size()); //nbDraw = nb of vertices
}
