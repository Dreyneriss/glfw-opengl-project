# PRESENTATION

I initially followed the tutorial from [learnopengl.com] to learn OpenGL with GLFW.  
For example, the two files camera.h and shader_m.h are directly taken from this tutorial (I didn't write them).  

Globaly, see this as an ugly experimenting project to learn OpenGL.

# INSTALLATION AND RUN

Some packages must be installed before-hand.  
`sudo apt-get install glfw3-dev libglm-dev libxi-dev`

One every librairy installed, just type  
`make`  
then run  
`./bin/prog`

# KEYS

WASD / ZQSD : move forward, backward and side strafe  
GLFW doesn't care whether you're in AZERTY or QWERTY

Mouse : view

Space & Ctrl : go Up & Down

F : switch between two "Move forward" methods

# Coming next

Adding more classical textured entities (rectangle boxes or walls individually textured)  
Using rotations : until then, I kept that appart  
Lighting stuffs :
- Stars colors evolving with time (not hard to do, but wanna do it in a clean way)
- Directional lighting : didn't go that far in the tutorials
- Cast Shadows
