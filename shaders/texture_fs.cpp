#version 330 core
out vec4 FragColor;

in vec2 TexCoord;

in vec3 Normal;
in vec3 FragPos;

// texture
uniform int nbText;
uniform float textureZoom;

uniform sampler2D texture1;
uniform sampler2D texture2;
uniform float specularity1;
uniform float specularity2;

// lights
uniform vec3 ambiantLight;

uniform vec3 lightPos1;
uniform vec3 lightPos2;
uniform vec3 lightColor1;
uniform vec3 lightColor2;
uniform vec3 viewPos;

void main()
{
	// ambient
	float ambientStrength = 0.1; //could be an uniform
	vec3 ambient = ambientStrength * ambiantLight;

	// diffuse light vars
	vec3 norm;
	vec3 lightDir;
	float diff;
	vec3 diffuse = vec3(0.f,0.f,0.f);

	// specular light vars
	int shininess = 64;

	vec3 viewDir;
	vec3 reflectDir;
	float spec;
	vec3 specular1; //texture1's specularity
	vec3 specular2; //texture2's specularity

	//#############
	// light 1

	// diffuse
	norm = normalize(Normal);

	lightDir = normalize(lightPos1 - FragPos);
	diff = max(dot(norm, lightDir), 0.0);
	diffuse = diff * lightColor1;

	// specular
	viewDir = normalize(viewPos - FragPos);

	reflectDir = reflect(-lightDir, norm);
	spec = pow(max(dot(viewDir, reflectDir), 0.0), shininess);
	specular1 = specularity1 * spec * lightColor1;
	specular2 = specularity2 * spec * lightColor1;

	//#############
	// light 2

	lightDir = normalize(lightPos2 - FragPos);
	diff = max(dot(norm, lightDir), 0.0);
	diffuse += diff * lightColor2;

	reflectDir = reflect(-lightDir, norm);
	spec = pow(max(dot(viewDir, reflectDir), 0.0), shininess);
	specular1 += specularity1 * spec * lightColor2;
	specular2 += specularity2 * spec * lightColor2;

	// mixed textures

	FragColor  = texture(texture1, TexCoord*textureZoom)*vec4(ambient+diffuse+specular1, 1.0);
	if(nbText>1)
		FragColor += texture(texture2, TexCoord*textureZoom)*vec4(ambient+diffuse+specular2, 1.0);
}
