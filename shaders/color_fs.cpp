#version 330 core
out vec4 FragColor;

in vec3 Normal;
in vec3 FragPos;

uniform vec3 objectColor;
uniform float specularity;

uniform vec3 ambiantLight;

uniform vec3 lightPos1;
uniform vec3 lightPos2;
uniform vec3 lightColor1;
uniform vec3 lightColor2;
uniform vec3 viewPos;

void main()
{
    // ambient
    float ambientStrength = 0.1;
    vec3 ambient = ambientStrength * lightColor1 * lightColor2;

    // diffuse
    vec3 norm = normalize(Normal);
    vec3 lightDir = normalize(lightPos1 - FragPos);
    float diff = max(dot(norm, lightDir), 0.0);
    vec3 diffuse = diff * lightColor1;

    // specular
    int shininess = 64;
    vec3 viewDir = normalize(viewPos - FragPos);
    vec3 reflectDir = reflect(-lightDir, norm);
    float spec = pow(max(dot(viewDir, reflectDir), 0.0), shininess);
    vec3 specular = specularity * spec * lightColor1;

    //LIGHT 2
    // diffuse
    norm = normalize(Normal);
    lightDir = normalize(lightPos2 - FragPos);
    diff = max(dot(norm, lightDir), 0.0);
    diffuse += diff * lightColor2;

    // specular
    shininess = 64;
    viewDir = normalize(viewPos - FragPos);
    reflectDir = reflect(-lightDir, norm);
    spec = pow(max(dot(viewDir, reflectDir), 0.0), shininess);
    specular += specularity * spec * lightColor2;

    vec3 result = (ambient + diffuse + specular) * objectColor;

    FragColor = vec4(result, 1.0);

}
