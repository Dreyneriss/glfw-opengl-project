#ifndef ENTITY_HPP
#define ENTITY_HPP

#include <iostream>
#include <fstream>
#include <sstream>

#include <stdio.h>	  /* printf, scanf, puts, NULL */
#include <stdlib.h>	 /* srand, rand */ // sudo apt-get install libxi-dev
#include <time.h>	   /* time */
#include <vector>
#include <cmath>

#include <glm/glm.hpp> // sudo apt-get install libglm-dev
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "shader_m.h"
#include "Frame.hpp"

//#####################
// BASE CLASS

class Entity
{
protected:
	Frame * _frame;
	Shader * _shader;

	glm::vec3 _coords;
	glm::vec3 _scale;
	glm::vec3 _axis;
	float _angle;

public:
	Entity(){}
	Entity(Frame * frame, Shader * shader);
	Entity(Frame * frame, Shader * shader, glm::vec3 pos, glm::vec3 scale, glm::vec3 axis, float angle);

	glm::vec3 getPos(){return _coords;}
	void setPos(glm::vec3 pos){_coords = pos;}
	void move(glm::vec3 dist){_coords += dist;}

	glm::vec3 getScale(){return _scale;}
	void setScale(glm::vec3 scale){_scale = scale;}
	void reScale(glm::vec3 scale){
		_scale.x *= scale.x;
		_scale.y *= scale.y;
		_scale.z *= scale.z;
	}

	glm::vec3 getAxis(){return _axis;}
	float getAngle(){return _angle;}
	void setOrientation(glm::vec3 axis, float angle){
		_axis = axis;
		_angle = angle;
	}
	void addAngle(float angle){_angle += angle;}

	virtual void draw(){}

};


//#####################
// TEXTURED ENTITY

class TexturedEntity : Entity
{
protected:
	int _nbText;
	//unsigned int* _textures;
	unsigned int _textures[2];
	//float* _speculars;
	float _speculars[2];

	float _textureZoom;

public:
	TexturedEntity();
	TexturedEntity(Frame * frame, Shader * shader);
	TexturedEntity(Frame * frame, Shader * shader, glm::vec3 pos, glm::vec3 scale, glm::vec3 axis, float angle);

	//void addTexture(unsigned int texture, float specular);
	void setTextures(unsigned int text1, float spec1, unsigned int text2, float spec2);
	void setLoneTexture(unsigned int text, float spec);
	void setNbText(int nb){_nbText = nb;}
	void setTextureZoom(float zoom){_textureZoom = zoom;}

	void draw();
};


//#####################
// SPRITED ENTITY

class SpritedEntity : Entity
{
protected:
	int _nbText;
	unsigned int _facesText[6][2]; //-1 meens no texture, and would be used for a second
	float _facesSpecs[6][2]; //specularities
	unsigned int* _textures;

public:
	SpritedEntity(Frame * frame, Shader * shader, glm::vec3 pos, glm::vec3 scale, glm::vec3 axis, float angle);

	void draw();
};


//#####################
// COLORED ENTITY

/*class ColoredEntity : Entity
{
protected:

public:
	void draw();
};*/


#endif
