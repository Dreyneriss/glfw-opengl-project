#ifndef FRAME_HPP
#define FRAME_HPP

#include <iostream>
#include <fstream>
#include <sstream>

#include <stdio.h>	  /* printf, scanf, puts, NULL */
#include <stdlib.h>	 /* srand, rand */ // sudo apt-get install libxi-dev
#include <time.h>	   /* time */
#include <vector>
#include <cmath>

//#####################
// BASE CLASS

class Frame
{
private:
	std::vector<float> _vertices;
	unsigned int _VAO;
	unsigned int _VBO;
	unsigned int _EBO;


public:
	Frame();
	Frame(std::string adress);

	std::vector<float> getVertices(){return _vertices;}
	unsigned int & getVAO(){return _VAO;}
	unsigned int & getVBO(){return _VBO;}
	unsigned int & getEBO(){return _EBO;}

};

//#####################
// SQUARED FRAME

/*class SquaredFrame : Frame
{
public:
	SquaredFrame();
	enum orientation{
		FRONT,
		BACK,
		LEFT,
		RIGHT,
		UP,
		DOWN
	};
};*/


#endif
